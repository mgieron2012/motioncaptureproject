#include "camera.h"

Camera::Camera(Qt3DRender::QCamera *camera)
    :cameraEntity(camera)
{
    cameraEntity->lens()->setPerspectiveProjection(45.0f, 16.0f/9.0f, 0.1f, 1000.0f);
    cameraEntity->setPosition(QVector3D(0, 0, 20.0f));
    cameraEntity->setUpVector(QVector3D(0, 1, 0));
    cameraEntity->setViewCenter(QVector3D(0, 0, 0));
    cameraEntity->viewSphere(QVector3D(0,0,0), 3);
    mousePressed = false;
}

void Camera::rotateCamera(QMouseEvent *event){
    if(!mousePressed) return;
    //cameraEntity->rotateAboutViewCenter(QQuaternion::fromAxisAndAngle(lastMousePosition.y() - event->y(), lastMousePosition.x() - event->x(), 0, 2));
    cameraEntity->panAboutViewCenter(lastMousePosition.x() - event->x());
    //cameraEntity->tiltAboutViewCenter(lastMousePosition.y() - event->y());
    lastMousePosition = event->pos();
}

void Camera::mousePressEvent(QMouseEvent *event){
    mousePressed = true;
    lastMousePosition = event->pos();
}

void Camera::mouseReleaseEvent(){
    lastMousePosition.setX(-1);
    mousePressed = false;
}
