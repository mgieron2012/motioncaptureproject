#include "mainwindow.h"
#include "camera.h"
#include "networkserver.h"
#include "light.h"
#include "skeleton.h"

#include <QApplication>
#include <Qt3DCore/qentity.h>
#include <QHBoxLayout>
#include <QWidget>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    //QQuaternion q1, q2;

    //q1 = QQuaternion::fromEulerAngles(0,50,0);
    //q2 = QQuaternion::fromEulerAngles(50,0,0);
    //qDebug()<<Utils::globalQuaternionToLocal(q1, q2).toEulerAngles();
    //qDebug()<<Utils::QuatToDirection(q1) - Utils::QuatToDirection(q2);
    //qDebug() << Utils::GetRotationInternal(QQuaternion::fromEulerAngles(50,0,0), QQuaternion::fromEulerAngles(0,50,0), 0.75).toEulerAngles();

    auto window = new MainWindow;
    auto rootEntity = new Qt3DCore::QEntity;
    auto skeleton = new Skeleton(rootEntity);
    auto network = new NetworkServer;

    window->createLayout(skeleton, network);
    window->defaultFrameGraph()->setClearColor(QColor(QRgb(0xaaaaaa)));

    auto camera = new Camera(window->camera());
    window->addCamera(camera);
    auto light = new Light(rootEntity, QVector3D(1,1,1));
    auto extraLight = new Light(rootEntity, QVector3D(-1,-1,-1));

    QObject::connect(network, &NetworkServer::infoReady, skeleton, &Skeleton::getInfo);

    window->setRootEntity(rootEntity);
    window->show();

    return a.exec();
}
