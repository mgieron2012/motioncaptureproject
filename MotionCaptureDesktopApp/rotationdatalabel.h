#ifndef ROTATIONDATALABEL_H
#define ROTATIONDATALABEL_H

#include <QLabel>
#include <QVector3D>

class RotationDataLabel : public QLabel
{
    Q_OBJECT

    int id;
    bool isActive;
    QVector3D *rotation;

public:
    RotationDataLabel(QWidget*, int);
    void setActive(bool);
    void refresh();

public slots:
    void setRotation(float, float, float);
};

#endif // ROTATIONDATALABEL_H
