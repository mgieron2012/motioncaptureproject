#include "bone.h"

Bone::Bone(Qt3DCore::QEntity *rootEntity, Bone* parent):
    parent(parent),
    isStatic(false),
    childType(ChildType::toptop)
{
    //Creating mesh
    mesh = new Qt3DRender::QMesh;
    mesh->setSource(QUrl::fromLocalFile(QDir::currentPath() + "/Assets/bone.stl"));

    //Creating transform
    transform = new Qt3DCore::QTransform();
    transform->setScale(1.5f);
    setRotation(270,90,90);
    transform->setTranslation(QVector3D(0.f, 1.0f, 0.f));

    //Creating material
    material = new Qt3DExtras::QPhongMaterial();
    material->setDiffuse(QColor(QRgb(0x928327)));

    //Creating entity
    entity = new Qt3DCore::QEntity(rootEntity);
    entity->addComponent(transform);
    entity->addComponent(material);
    entity->addComponent(mesh);

    //Setting parent
    if(parent)
    parent->addChild(this);
}

void Bone::setPosition(float x, float y, float z){
    transform->setTranslation(QVector3D(x, y, z));

    for(auto child : children)
        child->setPosition();
}

void Bone::setPosition(){
    if(!parent) return;

    switch(childType){
    case ChildType::toptop:
        transform->setTranslation(
        parent->getPosition() + Utils::QuatToDirection(parent->getRotation()) * parent->getScale(1.1f)
                    - Utils::QuatToDirection(getRotation()) * getScale(1.1f));
    break;

    case ChildType::topbottom:
        transform->setTranslation(
        parent->getPosition() + Utils::QuatToDirection(parent->getRotation()) * parent->getScale(1.1f)
                    + Utils::QuatToDirection(getRotation()) * getScale(1.1f));
    break;

    case ChildType::bottomtop:
        transform->setTranslation(
        parent->getPosition() - Utils::QuatToDirection(parent->getRotation()) * parent->getScale(1.1f)
                    - Utils::QuatToDirection(getRotation()) * getScale(1.1f));
    break;

    case ChildType::bottombottom:
        transform->setTranslation(
        parent->getPosition() - Utils::QuatToDirection(parent->getRotation()) * parent->getScale(1.1f)
                    + Utils::QuatToDirection(getRotation()) * getScale(1.1f));
    break;
    }

    for(auto child : children)
        child->setPosition();
}

void Bone::setScale(float scale){
    transform->setScale(scale);
}

void Bone::setRotation(float x, float y, float z){
    if(isStatic) return;
    transform->setRotation(QQuaternion::fromEulerAngles(x, y, z));
    setPosition();
    emit rotationChanged(x, y, z);
}

void Bone::setActive(bool isActive){
    if(isActive) material->setDiffuse(QColor(QRgb(0xff0000)));
    else material->setDiffuse(QColor(QRgb(0x00ff00)));

    entity->addComponent(material);
}

QVector3D Bone::getPosition(){
    return transform->translation();
}

QQuaternion Bone::getRotation(){
    return transform->rotation();
}

float Bone::getScale(float multiplier = 1){
    return transform->scale() * multiplier;
}

void Bone::setStatic(bool isStatic){
    this->isStatic = isStatic;
}

void Bone::setChildType(ChildType type){
    this->childType = type;
}

void Bone::addChild(Bone* child){
    this->children.push_front(child);
}
