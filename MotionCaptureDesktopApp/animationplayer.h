#ifndef ANIMATIONPLAYER_H
#define ANIMATIONPLAYER_H

#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QPushButton>
#include <QSlider>
#include <QLabel>
#include <QLineEdit>
#include <QTimer>
#include "databasemanager.h"
#include "skeleton.h"


class AnimationPlayer: public QVBoxLayout
{
    Q_OBJECT

    int currentPlayingSaveId;
    int animationTime;
    bool animationPaused;
    QHBoxLayout* animationNameEditLayout;
    QHBoxLayout* buttonsLayout;
    QHBoxLayout* sliderLayout;

    QPushButton* playButton;
    QPushButton* pauseButton;
    QPushButton* stopButton;
    QSlider* slider;
    QLabel* currentTime;
    QLabel* animationLength;
    QLineEdit* animationSaveName;
    QVector<RotationDBObject> playingAnimPoints;
    Skeleton *skeleton;
    QLabel *myName;

public:
    AnimationPlayer(Skeleton*);
    QSlider* getSlider();

signals:
    void animationChanged(int, int);
    void reloadSaveLabels();
    void networkSwitch(bool);

public slots:
    void animationSaveChose(int, QString);
    void playAnimation();
    void updateSaveName();
    void playAnimationFrame();
    void pauseAnimation();
    void stopAnimation();
    void goToTime(int);
    void reloadAnimationPlayer();
};

#endif // ANIMATIONPLAYER_H
