#include "skeleton.h"

Skeleton::Skeleton(Qt3DCore::QEntity *rootEntity)
{
    bones.push_front(new Bone(rootEntity, nullptr));
    //bones.first()->setRotation(0,0,0);
    bones.first()->setScale(1.f);

    bones.push_front(new Bone(rootEntity, bones.last()));
    bones.first()->setScale(0.4f);
    bones.first()->setChildType(Bone::ChildType::topbottom);

    bones.push_front(new Bone(rootEntity, bones.last()));
    bones.first()->setScale(0.2f);
    bones.first()->setChildType(Bone::ChildType::toptop);
    bones.first()->setRotation(-20,0,0);
    bones.first()->setStatic(true);

    bones.push_front(new Bone(rootEntity, bones.first()));
    bones.first()->setScale(0.55f);
    bones.first()->setChildType(Bone::ChildType::bottomtop);

    bones.push_front(new Bone(rootEntity, bones.first()));
    bones.first()->setScale(0.5f);
    bones.first()->setChildType(Bone::ChildType::bottomtop);

    bones.push_front(new Bone(rootEntity, bones.last()));
    bones.first()->setScale(0.2f);
    bones.first()->setChildType(Bone::ChildType::toptop);
    bones.first()->setRotation(-160.0f,0,0);
    bones.first()->setStatic(true);

    bones.push_front(new Bone(rootEntity, bones.first()));
    bones.first()->setScale(0.55f);
    bones.first()->setChildType(Bone::ChildType::bottomtop);

    bones.push_front(new Bone(rootEntity, bones.first()));
    bones.first()->setScale(0.5f);
    bones.first()->setChildType(Bone::ChildType::bottomtop);

    bones.push_front(new Bone(rootEntity, bones.last()));
    bones.first()->setScale(0.15f);
    bones.first()->setChildType(Bone::ChildType::bottomtop);
    bones.first()->setRotation(-25.0f,0,0);
    bones.first()->setStatic(true);

    bones.push_front(new Bone(rootEntity, bones.first()));
    bones.first()->setScale(0.55f);
    bones.first()->setChildType(Bone::ChildType::bottomtop);

    bones.push_front(new Bone(rootEntity, bones.first()));
    bones.first()->setScale(0.5f);
    bones.first()->setChildType(Bone::ChildType::bottomtop);

    bones.push_front(new Bone(rootEntity, bones.last()));
    bones.first()->setScale(0.15f);
    bones.first()->setChildType(Bone::ChildType::bottomtop);
    bones.first()->setRotation(-155.0f,0,0);
    bones.first()->setStatic(true);

    bones.push_front(new Bone(rootEntity, bones.first()));
    bones.first()->setScale(0.55f);
    bones.first()->setChildType(Bone::ChildType::bottomtop);

    bones.push_front(new Bone(rootEntity, bones.first()));
    bones.first()->setScale(0.5f);
    bones.first()->setChildType(Bone::ChildType::bottomtop);

    bones.last()->setPosition(0,1,0);

    /////Bones id draw
    ///
    ///     13
    ///     13
    /// 1212140909
    /// 11  14  08
    /// 11  14  08
    /// 10  14  07
    /// 10  14  07
    ///   061403
    ///   05  02
    ///   05  02
    ///   04  01
    ///   04  01
    ///
    /////Bones names for model
    ///
    /// 1: LeftLeg
    /// 2: LeftLegUp
    /// 4: RightLeg
    /// 5: RightLegUp
    /// 7: LeftForeArm
    /// 8: LeftArm
    /// 10: RightForeArm
    /// 11: Rightarm
    /// 13: Head
    /// 14: Spine
}

Bone* Skeleton::getBone(int id){
    return this->bones.at(id-1);
}

void Skeleton::getInfo(int id, float x, float y, float z){
    getBone(id)->setRotation(x, y, z);
}

void Skeleton::setDefaultRotation(){
    for(Bone *bone : bones){
       bone->setRotation(270,90,90);
    }
}
