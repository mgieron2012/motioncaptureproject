#ifndef ANIMATIONSMANAGER_H
#define ANIMATIONSMANAGER_H

#include <QVBoxLayout>
#include <QScrollArea>
#include <QFileDialog>
#include <animationplayer.h>
#include <animationeditor.h>
#include <savelabel.h>

class AnimationsManager : public QVBoxLayout
{
    Q_OBJECT

    AnimationPlayer *animationPlayer;
    AnimationEditor *animationEditor;

    QScrollArea* scrollArea;
    QVBoxLayout* scrollVLayout;
    QWidget* emergencyWidget;
    QPushButton* newAnimationButton;

    QString parseRawExportData(QString, QString, int, QString = "", QString = "", QString = "", QString = "", int = 0);
    QString parseRawExportData(QString, QString, int, QString, QString, int, QString);


public:
    AnimationsManager(Skeleton*, NetworkServer*);

    void loadSaveLabels();
    void initializeScrollArea();
    void createAnimation();
    void exportToFile(int);    

};

#endif // ANIMATIONSMANAGER_H
