#include "animationsaver.h"

AnimationSaver::AnimationSaver(int saveId, NetworkServer* server)
    :saveId(saveId)
{
    timer.start();
    QObject::connect(server, &NetworkServer::infoReady, this, &AnimationSaver::addFrame);
    qDebug()<<timer.elapsed();
}

void AnimationSaver::addFrame(int boneId, float x, float y, float z){
    DatabaseManager::getInstance()->addNewRotationRecord(RotationDBObject(saveId, boneId, timer.elapsed(), x, y, z));
}
