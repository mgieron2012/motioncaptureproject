#ifndef SAVELABEL_H
#define SAVELABEL_H

#include <QLabel>
#include <QPushButton>
#include <animationsaver.h>

class SaveLabel : public QLabel
{
    Q_OBJECT

    int id;
    void mousePressEvent(QMouseEvent*) override;


public:
    SaveLabel(int, QString);

signals:
    void clicked(int, QString);
    void exportAnimation(int);

public slots:
    void exportClicked();
};

#endif // SAVELABEL_H
