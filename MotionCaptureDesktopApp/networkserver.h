#ifndef NETWORKSERVER_H
#define NETWORKSERVER_H

#include <QtNetwork>
#include <QObject>
#include "utils.h"

class NetworkServer : public QObject
{
    Q_OBJECT

    QUdpSocket udpSocket4;
    QHostAddress groupAddress4;
    bool isActive;

private slots:
    void processPendingDatagrams();
public slots:
    void setActive(bool);

public:
    signals:
        void infoReady(int, float, float, float);

public:
    NetworkServer();
private:
    void encodeMessage(const QByteArray&);

};

#endif // NETWORKSERVER_H
