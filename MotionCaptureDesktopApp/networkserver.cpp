#include "networkserver.h"

NetworkServer::NetworkServer()
    : groupAddress4(QStringLiteral("0.0.0.0")),
      isActive(true)
{
    udpSocket4.bind(QHostAddress::AnyIPv4, 45454, QUdpSocket::ShareAddress);
    udpSocket4.joinMulticastGroup(groupAddress4);

    QObject::connect(&udpSocket4, SIGNAL(readyRead()),
            this, SLOT(processPendingDatagrams()));
}

void NetworkServer::processPendingDatagrams()
{
    if(!isActive) return;

    QByteArray datagram;
    while (udpSocket4.hasPendingDatagrams()) {
        datagram.resize(int(udpSocket4.pendingDatagramSize()));
        udpSocket4.readDatagram(datagram.data(), datagram.size());
    }

    encodeMessage(datagram);
}

void NetworkServer::encodeMessage(const QByteArray &message){
    QJsonObject *messageObject = new QJsonObject(Utils::QByteArrayToJsonObject(message));
    if(messageObject->contains("id") && messageObject->contains("x") && messageObject->contains("y") && messageObject->contains("z")){
        emit infoReady(messageObject->find("id").value().toInt(),
                       static_cast<float>(messageObject->find("x").value().toDouble()),
                       static_cast<float>(messageObject->find("y").value().toDouble()),
                       static_cast<float>(messageObject->find("z").value().toDouble()));
    } else {
        qDebug()<<"Odebrano nieprawidłowe dane";
        return;
    }
}

void NetworkServer::setActive(bool active){
    isActive = active;
}
