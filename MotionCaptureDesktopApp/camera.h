#ifndef CAMERA_H
#define CAMERA_H

#include <Qt3DRender/QCamera>

#include <QMouseHandler>

class Camera : public QObject
{
    QPoint lastMousePosition;
    bool mousePressed;

public:
    Camera(Qt3DRender::QCamera*);
    Qt3DRender::QCamera *cameraEntity;

    void rotateCamera(QMouseEvent*);
    void mousePressEvent(QMouseEvent*);
    void mouseReleaseEvent();
};

#endif // CAMERA_H
