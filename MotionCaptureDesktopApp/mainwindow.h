#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <Qt3DExtras/Qt3DWindow>
#include <QScreen>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QScrollArea>
#include "rotationdatalabel.h"
#include "skeleton.h"
#include "camera.h"
#include "databasemanager.h"
#include "savelabel.h"
#include "animationsmanager.h"

class MainWindow : public Qt3DExtras::Qt3DWindow {

    Q_OBJECT

    QWidget *container;
    QSize screenSize;
    Camera *myCamera;
    QWidget* widget;
    AnimationsManager* animationManager;
    QHBoxLayout *mainHLayout;
    QVBoxLayout *vLayout;
    QHBoxLayout *hLayout;
    QHBoxLayout *hLayout2;
    QVector<RotationDataLabel*> rotationLabels;

public:
    explicit MainWindow();

    void createLayout(Skeleton *skeleton, NetworkServer*);

    void mouseMoveEvent(QMouseEvent*);
    void mouseReleaseEvent(QMouseEvent*);
    void mousePressEvent(QMouseEvent*);
    void addCamera(Camera*);
};

#endif // MAINWINDOW_H
