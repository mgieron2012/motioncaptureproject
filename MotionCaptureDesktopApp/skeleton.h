#ifndef SKELETON_H
#define SKELETON_H

#include "bone.h"

class Skeleton: public QObject
{
    QVector<Bone*> bones;

public:
    Skeleton(Qt3DCore::QEntity*);

    Bone* getBone(int);

public slots:
    void getInfo(int, float, float, float);
    void setDefaultRotation();
};

#endif // SKELETON_H
