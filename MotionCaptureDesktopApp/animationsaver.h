#ifndef ANIMATIONSAVER_H
#define ANIMATIONSAVER_H

#include <QTime>
#include <databasemanager.h>
#include <networkserver.h>

class AnimationSaver : public QObject
{
    Q_OBJECT

    int saveId;
    QTime timer;

public:
    AnimationSaver(int, NetworkServer*);

    void addFrame(int, float, float, float);
};

#endif // ANIMATIONSAVER_H
