#ifndef ANIMATIONEDITOR_H
#define ANIMATIONEDITOR_H

#include <QLabel>
#include <QSlider>
#include <QPushButton>
#include <QVBoxLayout>
#include <QHBoxLayout>

#include <animationsaver.h>
#include <networkserver.h>

class AnimationEditor : public QVBoxLayout
{
    Q_OBJECT

    QHBoxLayout *buttonsLayout;
    QPushButton *deleteButton;
    QPushButton *cutButton;
    QPushButton *startButton;
    AnimationSaver *animationSaver;
    QLabel *myName;
    int currentSaveId;
    NetworkServer *server;

public:

    std::pair<QSlider*, QSlider*> slider;

    AnimationEditor(NetworkServer*);
signals:
    void cutDone();
    void reloadSaveLabels();
    void networkSwitch(bool);

public slots:
    void startSaving();
    void stopSaving();
    void deleteSave();
    void onStartClicked();
    void createLayout();
    void onCutClicked();
    void cutAnimation(int, int, int);
    void animationChanged(int, int);

};

#endif // ANIMATIONEDITOR_H
