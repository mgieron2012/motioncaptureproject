#ifndef UTILS_H
#define UTILS_H

#include <QQuaternion>
#include <math.h>
#include <QMatrix4x4>
#include <QJsonDocument>
#include <QJsonObject>
#include <QFile>

class Utils
{

public:
    Utils();

    static QVector3D QuatToDirection(QQuaternion);
    static QJsonObject QByteArrayToJsonObject(const QByteArray&);
    static QString msecToTime(int);
    static QJsonObject readJsonFromFile(QString);
    static QQuaternion globalQuaternionToLocal(QQuaternion, QQuaternion);
    static QQuaternion GetRotationInternal(QQuaternion, QQuaternion, float);
    static QMatrix4x4 OrthogonalizeMatrix(QMatrix4x4);
    static QQuaternion QuaternionFromMatrix(QMatrix4x4);


};

#endif // UTILS_H
