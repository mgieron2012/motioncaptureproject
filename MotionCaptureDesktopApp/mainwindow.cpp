#include "mainwindow.h"

MainWindow::MainWindow() = default;

void MainWindow::createLayout(Skeleton *skeleton, NetworkServer* server){

    //Creating layout
    container = QWidget::createWindowContainer(this);
    container->setMinimumSize(QSize(800, 600));
    container->setMaximumSize(this->screen()->size());
    widget = new QWidget;

    /*Layouts positions
     *
     * MainHLayout
     * ------------------------------
     * |vLayout         |animationManager|
     * |----------------|-----------|
     * ||container3D    ||ScrollArea|
     * ||         <-----||----------|----- Scene with 3D skeleton
     * ||               |||vLayout3 |
     * ||               |||   <-----|----- Saved animations poles
     * ||---------------|||         |
     * ||hLayout  <-----|||---------|----- Info about rotation from bones with ID 1-7
     * ||---------------|||         |
     * ||hLayout2 <-----|||---------|----- Info about rotation from bones with ID 8-14
     * ------------------------------
     * */

    mainHLayout = new QHBoxLayout(widget);
    vLayout = new QVBoxLayout();
    hLayout = new QHBoxLayout();
    hLayout2 = new QHBoxLayout();

    animationManager = new AnimationsManager(skeleton, server);

    mainHLayout->addLayout(vLayout);
    mainHLayout->addLayout(animationManager);

    vLayout->addWidget(container, Qt::AlignTop);
    vLayout->addLayout(hLayout);
    vLayout->addLayout(hLayout2);

    widget->setStyleSheet("QPushButton{padding: 7px; border-radius: 7px; background-color: #fff; font-size: 15px}");

    //Adding rotation labels
    int bonesNumbers[10] = {1,2,4,5,7,8,10,11,13,14};
    for(int i=0;i<5;i++){
        auto rotationWidget = new RotationDataLabel(widget, bonesNumbers[i]);
        rotationLabels.push_back(rotationWidget);
        rotationWidget->refresh();
        hLayout->addWidget(rotationWidget);
        QObject::connect(skeleton->getBone(bonesNumbers[i]), &Bone::rotationChanged, rotationWidget, &RotationDataLabel::setRotation);
    }

    for(int i=5;i<10;i++){
        auto rotationWidget = new RotationDataLabel(widget, bonesNumbers[i]);
        rotationLabels.push_back(rotationWidget);
        rotationWidget->refresh();
        hLayout2->addWidget(rotationWidget);
        QObject::connect(skeleton->getBone(bonesNumbers[i]), &Bone::rotationChanged, rotationWidget, &RotationDataLabel::setRotation);
    }

    widget->show();
    widget->resize(1200, 800);
}

void MainWindow::addCamera(Camera *camera){
    this->myCamera = camera;
}

void MainWindow::mouseMoveEvent(QMouseEvent *event)
{
    myCamera->rotateCamera(event);
}

void MainWindow::mouseReleaseEvent(QMouseEvent *event)
{
    myCamera->mouseReleaseEvent();
}

void MainWindow::mousePressEvent(QMouseEvent *event)
{
    myCamera->mousePressEvent(event);
}
