#include "savelabel.h"

SaveLabel::SaveLabel(int id, QString name):
    id(id)
{
    setText(name);
    setStyleSheet("QLabel {font-size:15px; color: #fff; margin-bottom: 5px; margin-right: 0px; padding: 10px 10px 10px 2px; border: 1px solid #29a4f3; border-top-left-radius: 15px; border-top-right-radius: 0px; border-bottom-right-radius: 0px; border-bottom-left-radius: 15px; background-color: #1c87ff;} QLabel:hover{background-color: #1692ff;}");
    setAlignment(Qt::AlignTop);
}

void SaveLabel::mousePressEvent(QMouseEvent* event){
    emit clicked(id, this->text());
}

void SaveLabel::exportClicked(){
    emit exportAnimation(id);
}
