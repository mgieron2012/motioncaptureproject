#include "animationplayer.h"

AnimationPlayer::AnimationPlayer(Skeleton *skeleton):
    currentPlayingSaveId(1),
    animationTime(0),
    animationPaused(false),
    skeleton(skeleton)
{
    myName = new QLabel();
    myName->setText("ANIMATION PREVIEW");
    addWidget(myName);
    myName->setStyleSheet("font-family: sans-serif; font-size: 17px; color: #2d3436;");

    animationNameEditLayout = new QHBoxLayout();
    buttonsLayout = new QHBoxLayout();
    sliderLayout = new QHBoxLayout();
    playButton = new QPushButton();
    playButton->setStyleSheet("QPushButton{background-color: #26de81; color: #fff;} QPushButton:hover{background-color: #30d187;}");
    pauseButton = new QPushButton();
    pauseButton->setStyleSheet("QPushButton{background-color: #f7b731; color: #fff;} QPushButton:hover{background-color: #ffaf38;}");
    stopButton= new QPushButton();
    stopButton->setStyleSheet("QPushButton{background-color: #eb3b5a; color: #fff;} QPushButton:hover{background-color: #c33b4a;}");
    slider = new QSlider();
    currentTime = new QLabel();
    animationLength = new QLabel();
    animationSaveName = new QLineEdit();
    animationSaveName->setStyleSheet("border-radius: 7px; padding: 5px 5px 5px 10px; font-size: 14px; margin-top: 10px;");

    this->addLayout(animationNameEditLayout);
    this->addLayout(sliderLayout);
    this->addLayout(buttonsLayout);

    animationNameEditLayout->addWidget(animationSaveName);

    buttonsLayout->addWidget(playButton);
    buttonsLayout->addWidget(pauseButton);
    buttonsLayout->addWidget(stopButton);

    sliderLayout->addWidget(currentTime);
    sliderLayout->addWidget(slider);
    sliderLayout->addWidget(animationLength);

    animationSaveName->setPlaceholderText("Animation name");
    playButton->setText("Play");
    pauseButton->setText("Pause");
    stopButton->setText("Stop");
    slider->setRange(0, 0);
    slider->setTickPosition(QSlider::TickPosition::NoTicks);
    slider->setOrientation(Qt::Orientation::Horizontal);

    currentTime->setText(Utils::msecToTime(0));
    animationLength->setText(Utils::msecToTime(0));

    QObject::connect(animationSaveName, &QLineEdit::editingFinished, this, &AnimationPlayer::updateSaveName);
    QObject::connect(playButton, &QPushButton::clicked, this, &AnimationPlayer::playAnimation);
    QObject::connect(pauseButton, &QPushButton::clicked, this, &AnimationPlayer::pauseAnimation);
    QObject::connect(stopButton, &QPushButton::clicked, this, &AnimationPlayer::stopAnimation);
    QObject::connect(slider, &QSlider::valueChanged, this, &AnimationPlayer::goToTime);
}

void AnimationPlayer::animationSaveChose(int id, QString name){
    currentPlayingSaveId = id;
    animationSaveName->setText(name);
    reloadAnimationPlayer();
}

void AnimationPlayer::reloadAnimationPlayer(){
    skeleton->setDefaultRotation();
    playingAnimPoints = DatabaseManager::getInstance()->getRotationsBySaveId(currentPlayingSaveId);
    if(!playingAnimPoints.isEmpty())
    slider->setRange(0, playingAnimPoints.last().time);
    else slider->setRange(0, 0);
    slider->setValue(0);
    if(!playingAnimPoints.isEmpty())
    animationLength->setText(Utils::msecToTime(playingAnimPoints.last().time));
    else animationLength->setText(Utils::msecToTime(0));
    emit animationChanged(currentPlayingSaveId, slider->maximum());
}

void AnimationPlayer::playAnimation(){
    if(animationPaused) animationPaused = false;
    if(currentTime->text() == animationLength->text()) goToTime(0);
    QTimer::singleShot(10, this, SLOT(playAnimationFrame()));
    emit networkSwitch(false);
}

void AnimationPlayer::updateSaveName(){
    DatabaseManager::getInstance()->updateSaveName(SaveDBObject(currentPlayingSaveId, animationSaveName->text()));
    emit reloadSaveLabels();
}

void AnimationPlayer::playAnimationFrame(){
    if(animationPaused || playingAnimPoints.isEmpty()) return;
    if(animationTime < playingAnimPoints.last().time){
        if(animationTime + 10 <= playingAnimPoints.last().time)
        {
            animationTime += 10;
        } else {
            animationTime = playingAnimPoints.last().time;
        }
        slider->setValue(animationTime);

        QTimer::singleShot(10, this, SLOT(playAnimationFrame()));
    } else {
        animationTime = 0;
    }

}

void AnimationPlayer::pauseAnimation(){
    animationPaused = true;
}

void AnimationPlayer::stopAnimation(){
    slider->setValue(0);
    animationPaused = true;
    emit networkSwitch(true);
}

void AnimationPlayer::goToTime(int time){
    if(time == 0) {
        skeleton->setDefaultRotation();
    } else {
        bool hasBoneChanged[14] = {0};
        skeleton->setDefaultRotation();
        for (int i=playingAnimPoints.length() - 1; i>=0; i--) {
            auto currentFrameObject = playingAnimPoints.at(i);

            if(currentFrameObject.time > time) continue;

            if(!hasBoneChanged[currentFrameObject.boneId]){
                hasBoneChanged[currentFrameObject.boneId] = true;
                skeleton->getBone(currentFrameObject.boneId)->setRotation(currentFrameObject.x, currentFrameObject.y, currentFrameObject.z);
            }
        }
    }
    animationTime = time;
    currentTime->setText(Utils::msecToTime(animationTime));
}

QSlider* AnimationPlayer::getSlider(){
    return slider;
}


