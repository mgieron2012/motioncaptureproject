#ifndef DATABASEMANAGER_H
#define DATABASEMANAGER_H

#include <QSqlDatabase>
#include <QSqlQuery>
#include <QDebug>
#include <QSqlError>
#include <QSqlRecord>

struct SaveDBObject{
    int id;
    QString name;
public:
    SaveDBObject(int id, QString name){
        this->id = id;
        this->name = name;
    }

    SaveDBObject(){
        this->id = 0;
        this->name = "";
    }
};

struct RotationDBObject{
    int saveId;
    int boneId;
    int time;
    float x;
    float y;
    float z;

public:
    RotationDBObject(){
        saveId = 0;
        boneId = 0;
        time = 0;
        x = 0;
        y = 0;
        z = 0;
    }

    RotationDBObject(int saveId, int boneId, int time, float x, float y, float z){
        this->saveId = saveId;
        this->boneId = boneId;
        this->time = time;
        this->x = x;
        this->y = y;
        this->z = z;
    }
};

class DatabaseManager
{
    QSqlDatabase database;
    static DatabaseManager *manager;
    int findNewestId();

    DatabaseManager(QString);

public:
    ~DatabaseManager();

    static DatabaseManager* getInstance();
    int addNewSave(QString);
    void deleteSave(int);
    void deleteRotation(int);
    void decreaseRotationTime(int, int);
    void deleteRotation(int, int, int);
    void addNewRotationRecord(RotationDBObject);
    QVector<SaveDBObject> getAllSaves();
    QVector<RotationDBObject> getRotationsBySaveId(int);
    QVector<RotationDBObject> getRotationsBySaveId(int, int);
    void updateSaveName(SaveDBObject);
};

#endif // DATABASEMANAGER_H
