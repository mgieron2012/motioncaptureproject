#include "databasemanager.h"

DatabaseManager::DatabaseManager(QString path)
{
    database = QSqlDatabase::addDatabase("QSQLITE");
    database.setDatabaseName(path);

    if(!database.open()) qDebug()<<"Couldn't connect to database";
}

DatabaseManager::~DatabaseManager()
{
    database.close();
}

int DatabaseManager::addNewSave(QString name)
{
   QSqlQuery query;
   query.prepare("INSERT INTO Save (id, name) VALUES (null, :name)");
   query.bindValue(":name", name);
   if(query.exec())
   {
        return findNewestId();
   }
   else
   {
        qDebug() << " error:  "
                 << query.lastError();
        return -1;
   }
}

void DatabaseManager::addNewRotationRecord(RotationDBObject object){
    QSqlQuery query;
    query.prepare("INSERT INTO Rotation (save_Id, bone_Id, time, x, y, z) VALUES (:save_Id, :bone_Id, :time, :x, :y, :z)");
    query.bindValue(":save_Id", object.saveId);
    query.bindValue(":bone_Id", object.boneId);
    query.bindValue(":time", object.time);
    query.bindValue(":x", object.x);
    query.bindValue(":y", object.y);
    query.bindValue(":z", object.z);

    if(!query.exec())
    {
        qDebug() << " Error:  "
                 << query.lastError();
    }
}

QVector<SaveDBObject> DatabaseManager::getAllSaves()
{
   auto saveObjects = new QVector<SaveDBObject>();
   QSqlQuery query("SELECT * FROM Save");
   while(query.next())
   {
       saveObjects->push_back(SaveDBObject(query.value(0).toInt(), query.value(1).toString()));
   }
    return *saveObjects;
}

QVector<RotationDBObject> DatabaseManager::getRotationsBySaveId(int saveId){
    auto saveObjects = new QVector<RotationDBObject>();
    QSqlQuery query;
    query.prepare("SELECT * FROM Rotation where save_Id=:saveId order by time");
    query.bindValue(":saveId", saveId);
    query.exec();
    while(query.next())
    {
        saveObjects->push_back(RotationDBObject(query.value(0).toInt(), query.value(1).toInt(), query.value(2).toInt(),
                                                query.value(3).toFloat(), query.value(4).toFloat(), query.value(5).toFloat()));
    }
     return *saveObjects;
}

QVector<RotationDBObject> DatabaseManager::getRotationsBySaveId(int saveId, int boneId){
    auto saveObjects = new QVector<RotationDBObject>();
    QSqlQuery query;
    query.prepare("SELECT * FROM Rotation where save_Id=:save_id AND bone_id=:bone_id order by time");
    query.bindValue(":save_id", saveId);
    query.bindValue(":bone_id", boneId);

    query.exec();
    while(query.next())
    {
        saveObjects->push_back(RotationDBObject(query.value(0).toInt(), query.value(1).toInt(), query.value(2).toInt(),
                                                query.value(3).toFloat(), query.value(4).toFloat(), query.value(5).toFloat()));
    }
     return *saveObjects;
}

int DatabaseManager::findNewestId(){
    QSqlQuery query("SELECT MAX(id) from Save");
    query.first();
    return query.value(0).toInt();
}

void DatabaseManager::updateSaveName(SaveDBObject newSave){
    QSqlQuery query;
    query.prepare("UPDATE Save SET name=:name WHERE id=:id");
    query.bindValue(":id", newSave.id);
    query.bindValue(":name", newSave.name);

    if(!query.exec())
    {
        qDebug() << " Error:  "
                 << query.lastError();
    }
}

void DatabaseManager::deleteSave(int id){
    QSqlQuery query;
    query.prepare("DELETE from Save WHERE id=:id");
    query.bindValue(":id", id);

    if(!query.exec())
    {
        qDebug() << " Error:  "
                 << query.lastError();
    }

    deleteRotation(id);
}

void DatabaseManager::deleteRotation(int saveId){
    QSqlQuery query;
    query.prepare("DELETE from Rotation WHERE save_Id=:saveId");
    query.bindValue(":saveId", saveId);

    if(!query.exec())
    {
        qDebug() << "Error:  "
                 << query.lastError();
    }
}

void DatabaseManager::decreaseRotationTime(int saveId, int time){
    QSqlQuery query;
    query.prepare("UPDATE Rotation SET time = time - :time WHERE save_Id=:saveId");
    query.bindValue(":saveId", saveId);
    query.bindValue(":time", time);

    if(!query.exec())
    {
        qDebug() << " Error:  "
                 << query.lastError();
    }
}

void DatabaseManager::deleteRotation(int saveId, int beggining, int end){
    QSqlQuery query;
    query.prepare("DELETE from Rotation WHERE save_Id=:saveId AND (time < :beggining OR time > :end)");
    query.bindValue(":saveId", saveId);
    query.bindValue(":beggining", beggining);
    query.bindValue(":end", end);

    if(!query.exec())
    {
        qDebug() << " Error:  "
                 << query.lastError();
    }

    decreaseRotationTime(saveId, beggining);
}

DatabaseManager* DatabaseManager::getInstance(){
    if(manager == nullptr){
        manager = new DatabaseManager("Assets\\base.db");
    }
    return manager;
}

DatabaseManager* DatabaseManager::manager = nullptr;


