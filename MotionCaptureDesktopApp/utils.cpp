#include "utils.h"

Utils::Utils() = default;

QVector3D Utils::QuatToDirection(QQuaternion q){
    float x = q.x();
    float y = q.y();
    float z = q.z();
    float w = q.scalar();
    return {
       2 * (x*z + w*y),
       2 * (y*z - w*x),
       1 - 2 * (x*x + y*y)};
}

QJsonObject Utils::QByteArrayToJsonObject(const QByteArray &dataIn){
    return QJsonDocument::fromJson(QString(dataIn).toUtf8()).object();
}

QString Utils::msecToTime(int msec){
    int min = msec / 60000;
    int sec = msec / 1000 % 60;
    int csec = msec / 10 % 100;

    QString minutes = QString::number(min);
    QString seconds = QString::number(sec);
    QString cseconds = QString::number(csec);

    if(sec < 10) seconds = "0" + seconds;
    if(csec < 10) cseconds = "0" + cseconds;

    if(min > 0) return minutes + ":" + seconds + ":" + cseconds;
    else return seconds + ":" + cseconds;
}

QJsonObject Utils::readJsonFromFile(QString path){
    QFile file(path);
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
        return QJsonObject();
    return QByteArrayToJsonObject(file.readAll());
}

QQuaternion Utils::globalQuaternionToLocal(QQuaternion global, QQuaternion parent){
    QMatrix4x4 mat;

    QQuaternion rot;
    QVector3D tangent = QVector3D(0, 1, 0);
    QVector3D normal = QVector3D(0, 0, 1);
/*
    for (int i = 1; i < nodes.Length; i++)
    {
        QVector3D upVec;

        if (i == nodes.Length - 1)
        {
            upVec = nodes[i].point - nodes[i - 1].point;
        }
        else
        {
            float distA = Vector3.Distance(nodes[i].point, nodes[i - 1].point);
            float distB = Vector3.Distance(nodes[i].point, nodes[i + 1].point);
            upVec = (nodes[i].point - nodes[i - 1].point) / distA + (nodes[i + 1].point - nodes[i].point) / distB;
        }
        upVec.normalize();

        mat.setColumn(1, upVec.toVector4D());
        if (abs(QVector3D::dotProduct(upVec, mat.column(0).toVector3D())) > 0.9999f)
        {
            mat.setColumn(0, QVector3D(0, 1, 0));
        }
        QVector3D c2 = QVector3D::crossProduct(mat.column(0).toVector3D(), upVec).normalized();
        mat.setColumn(2, c2);
        mat = OrthogonalizeMatrix(mat);

        nodes[i].rot = MathUtils.QuaternionFromMatrix(mat);
        nodes[i].normal = mat.GetColumn(2);
        nodes[i].tangent = mat.GetColumn(1);

        //
        // Make sure rotation is correct. As the same rotation can be
        // represented by two quaternions, make sure we are using the right one
        // otherwise knots appear as we interpolate over the spline
        //
        if (Quaternion.Dot(nodes[i].rot, nodes[i - 1].rot) < 0.0f)
        {
            nodes[i].rot.x = -nodes[i].rot.x;
            nodes[i].rot.y = -nodes[i].rot.y;
            nodes[i].rot.z = -nodes[i].rot.z;
            nodes[i].rot.w = -nodes[i].rot.w;
        }
    }

    qDebug()<< QuatToDirection(global) - QuatToDirection(parent);*/
    return QQuaternion::fromEulerAngles(0,0,0);
}


QQuaternion Utils::GetRotationInternal(QQuaternion Q0, QQuaternion Q2, float t)
{
    float t2 = t * t;
    float t3 = t2 * t;
    float tension = 0.5f;
    QQuaternion Q1 = Q2;
    QQuaternion Q3 = Q0;

    QQuaternion T1 = QQuaternion(tension * (Q2.x() - Q0.x()), tension * (Q2.y() - Q0.y()), tension * (Q2.z() - Q0.z()), tension * (Q2.scalar() - Q0.scalar()));
    QQuaternion T2 = QQuaternion(tension * (Q3.x() - Q1.x()), tension * (Q3.y() - Q1.y()), tension * (Q3.z() - Q1.z()), tension * (Q3.scalar() - Q1.scalar()));

    float Blend1 = 2 * t3 - 3 * t2 + 1;
    float Blend2 = -2 * t3 + 3 * t2;
    float Blend3 = t3 - 2 * t2 + t;
    float Blend4 = t3 - t2;

    QQuaternion q = QQuaternion();
    q.setX( Blend1 * Q1.x() + Blend2 * Q2.x() + Blend3 * T1.x() + Blend4 * T2.x());
    q.setY( Blend1 * Q1.y() + Blend2 * Q2.y() + Blend3 * T1.y() + Blend4 * T2.y());
    q.setZ( Blend1 * Q1.z() + Blend2 * Q2.z() + Blend3 * T1.z() + Blend4 * T2.z());
    q.setScalar(Blend1 * Q1.scalar() + Blend2 * Q2.scalar() + Blend3 * T1.scalar() + Blend4 * T2.scalar());
    float mag = sqrt(q.x() * q.x() + q.y() * q.y() + q.z() * q.z() + q.scalar() * q.scalar());
    q.setX(q.x() / mag);
    q.setY(q.y() / mag);
    q.setZ(q.z() / mag);
    q.setScalar(q.scalar() / mag);
    return q;
}

QMatrix4x4 Utils::OrthogonalizeMatrix(QMatrix4x4 m)
{
    QMatrix4x4 n = QMatrix4x4();

    QVector3D i = m.column(0).toVector3D();
    QVector3D j = m.column(1).toVector3D();
    QVector3D k = m.column(2).toVector3D();

    k = k.normalized();
    i = QVector3D::crossProduct(j, k).normalized();
    j = QVector3D::crossProduct(k, i).normalized();

    n.setColumn(0, i);
    n.setColumn(1, j);
    n.setColumn(2, k);

    return n;
}

int sign (float number) {
    return number < 0 ? -1 : 0;
}

QQuaternion Utils::QuaternionFromMatrix(QMatrix4x4 m)
{
    using namespace std;
    // Adapted from: http://www.euclideanspace.com/maths/geometry/rotations/conversions/matrixToQuaternion/index.htm
    QQuaternion q = QQuaternion();
    q.setScalar(sqrt(max(0.f, 1 + m(0, 0) + m(1, 1) + m(2, 2))) / 2);
    q.setX(sqrt(max(0.f, 1 + m(0, 0) - m(1, 1) - m(2, 2))) / 2);
    q.setY(sqrt(max(0.f, 1 - m(0, 0) + m(1, 1) - m(2, 2))) / 2);
    q.setZ(sqrt(max(0.f, 1 - m(0, 0) - m(1, 1) + m(2, 2))) / 2);
    q.setX(q.x() * sign(q.x() * (m(2, 1) - m(1, 2))));
    q.setY(q.y() * sign(q.y() * (m(0, 2) - m(2, 0))));
    q.setZ( q.z() * sign(q.z() * (m(1, 0) - m(0, 1))));
    return q.normalized();
}
