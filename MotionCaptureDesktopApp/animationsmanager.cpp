#include "animationsmanager.h"

AnimationsManager::AnimationsManager(Skeleton* skeleton, NetworkServer* server)
{
    animationPlayer = new AnimationPlayer(skeleton);
    animationEditor = new AnimationEditor(server);

    this->addLayout(animationPlayer);
    this->addLayout(animationEditor);

    initializeScrollArea();

    newAnimationButton = new QPushButton();
    newAnimationButton->setStyleSheet("background-color: #26de81; color: #fff;");
    newAnimationButton->setText("New animation");
    this->addWidget(newAnimationButton);

    QObject::connect(newAnimationButton, &QPushButton::clicked, this, &AnimationsManager::createAnimation);
    QObject::connect(animationEditor->slider.first, &QSlider::valueChanged, animationPlayer, &AnimationPlayer::goToTime);
    QObject::connect(animationEditor->slider.second, &QSlider::valueChanged, animationPlayer, &AnimationPlayer::goToTime);
    QObject::connect(animationEditor, &AnimationEditor::cutDone, animationPlayer, &AnimationPlayer::reloadAnimationPlayer);
    QObject::connect(animationPlayer, &AnimationPlayer::animationChanged, animationEditor, &AnimationEditor::animationChanged);
    QObject::connect(animationPlayer, &AnimationPlayer::reloadSaveLabels, this, &AnimationsManager::loadSaveLabels);
    QObject::connect(animationEditor, &AnimationEditor::reloadSaveLabels, this, &AnimationsManager::loadSaveLabels);
    QObject::connect(animationPlayer, &AnimationPlayer::networkSwitch, server, &NetworkServer::setActive);
    QObject::connect(animationEditor, &AnimationEditor::networkSwitch, server, &NetworkServer::setActive);
}

void AnimationsManager::loadSaveLabels(){
    for (auto label : emergencyWidget->findChildren<QPushButton *> ()){
        label->deleteLater();
    }
    for (auto label : emergencyWidget->findChildren<SaveLabel *> ()){
        label->clear();
        label->deleteLater();
    }
    for (auto label : emergencyWidget->findChildren<QHBoxLayout *> ()){
        label->deleteLater();
    }
    for (SaveDBObject object : DatabaseManager::getInstance()->getAllSaves()){
        auto label = new SaveLabel(object.id, object.name);
        auto layout = new QHBoxLayout();
        auto exportButton = new QPushButton("E\nX\nP\nO\nR\nT");
        exportButton->setMaximumWidth(50);
        layout->addWidget(label);
        layout->addWidget(exportButton);
        scrollVLayout->addLayout(layout);
        QObject::connect(label, &SaveLabel::clicked, animationPlayer, &AnimationPlayer::animationSaveChose);
        QObject::connect(label, &SaveLabel::exportAnimation, this, &AnimationsManager::exportToFile);
        QObject::connect(exportButton, &QPushButton::clicked, label, &SaveLabel::exportClicked);
    }
    emergencyWidget->updateGeometry();
}

void AnimationsManager::initializeScrollArea(){

    //if(scrollVLayout == nullptr)
    scrollVLayout = new QVBoxLayout();

    scrollVLayout->setSizeConstraint(QLayout::SetMinAndMaxSize);

    //if(emergencyWidget == nullptr)
    emergencyWidget = new QWidget();

    emergencyWidget->setLayout(scrollVLayout);
    emergencyWidget->setStyleSheet("QWidget{background-color: #0984e3;} QPushButton {margin-bottom: 5px; margin-left: 0px; background-color: #0000ff; color: #fff; border-top-left-radius: 0px; border-top-right-radius: 15px; border-bottom-right-radius: 15px; border-bottom-left-radius: 0px; padding: 5px 5px 5px 0px; } QPushButton:hover {background-color: #000099;}");

    //if(scrollArea == nullptr)
    scrollArea = new QScrollArea();
    scrollArea->setHorizontalScrollBarPolicy(Qt::ScrollBarPolicy::ScrollBarAlwaysOff);
    scrollArea->setVerticalScrollBarPolicy(Qt::ScrollBarPolicy::ScrollBarAsNeeded);
    scrollArea->setWidgetResizable(true);

    scrollArea->setWidget(emergencyWidget);

    this->addWidget(scrollArea);

    loadSaveLabels();
}

void AnimationsManager::createAnimation(){
    DatabaseManager::getInstance()->addNewSave("New animation");
    loadSaveLabels();
}

void AnimationsManager::exportToFile(int animationId){
    QVector<RotationDBObject> animPoints = DatabaseManager::getInstance()->getRotationsBySaveId(animationId);

    if(animPoints.isEmpty()) return;

    QString fileName = QFileDialog::getOpenFileName(this->emergencyWidget,
        tr("Choose file"), "../MotionCapture/Assets", tr("Animation files (*.anim)"));

    QFile file(fileName);

    if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
        return;

    QTextStream out(&file);
    out.setCodec("UTF-8");
    out.setGenerateByteOrderMark(true);
    QJsonObject exportData = Utils::readJsonFromFile("Assets\\exportInfo.json");

    int bonesNumbers[10] = {1,2,4,5,7,8,10,11,13,14};

    out << parseRawExportData(exportData.find("part1").value().toString(), fileName, animPoints.last().time);

    for(int i : bonesNumbers){
        out << parseRawExportData(exportData.find("part2").value().toString(), fileName, animPoints.last().time,
                                  exportData.find("startValuex" + QString::number(i)).value().toString(),
                                  exportData.find("startValuey" + QString::number(i)).value().toString(),
                                  exportData.find("startValuez" + QString::number(i)).value().toString(),
                                  exportData.find("path" + QString::number(i)).value().toString());
        out << parseRawExportData(exportData.find("part22").value().toString(), fileName, animPoints.last().time,
                                  exportData.find("startValuex" + QString::number(i)).value().toString(),
                                  exportData.find("startValuey" + QString::number(i)).value().toString(),
                                  exportData.find("startValuez" + QString::number(i)).value().toString(),
                                  exportData.find("path" + QString::number(i)).value().toString(), 0);

        for(auto rec : DatabaseManager::getInstance()->getRotationsBySaveId(animationId, i)){
        out << parseRawExportData(exportData.find("part22").value().toString(), fileName, animPoints.last().time,
                                  QString::number(static_cast<double>(rec.x)),
                                  QString::number(static_cast<double>(rec.y)),
                                  QString::number(static_cast<double>(rec.z)),
                                  exportData.find("path" + QString::number(i)).value().toString(), rec.time);
        }
        out << parseRawExportData(exportData.find("part22").value().toString(), fileName, animPoints.last().time,
                                  exportData.find("startValuex" + QString::number(i)).value().toString(),
                                  exportData.find("startValuey" + QString::number(i)).value().toString(),
                                  exportData.find("startValuez" + QString::number(i)).value().toString(),
                                  exportData.find("path" + QString::number(i)).value().toString(), animPoints.last().time);

        out << parseRawExportData(exportData.find("part23").value().toString(), fileName, animPoints.last().time,
                                  exportData.find("startValuex" + QString::number(i)).value().toString(),
                                  exportData.find("startValuey" + QString::number(i)).value().toString(),
                                  exportData.find("startValuez" + QString::number(i)).value().toString(),
                                  exportData.find("path" + QString::number(i)).value().toString(), animPoints.last().time);
    }

    out << parseRawExportData(exportData.find("part3").value().toString(), fileName, animPoints.last().time);

    for(int i : bonesNumbers){
        out << exportData.find("part4").value().toString().toUtf8();
        out << parseRawExportData(exportData.find("part5").value().toString(), fileName, animPoints.last().time,
                                  exportData.find("startValuex" + QString::number(i)).value().toString(),
                                  exportData.find("path" + QString::number(i)).value().toString(),
                                  0,
                                  "x");
        for(auto rec : DatabaseManager::getInstance()->getRotationsBySaveId(animationId, i)){
            out << parseRawExportData(exportData.find("part5").value().toString(), fileName, animPoints.last().time,
                                      QString::number(static_cast<double>(rec.x)),
                                      exportData.find("path" + QString::number(i)).value().toString(),
                                      rec.time,
                                      "x");
        }

        out << parseRawExportData(exportData.find("part5").value().toString(), fileName, animPoints.last().time,
                                  exportData.find("startValuex" + QString::number(i)).value().toString(),
                                  exportData.find("path" + QString::number(i)).value().toString(),
                                  animPoints.last().time,
                                  "x");

        out << parseRawExportData(exportData.find("part6").value().toString(), fileName, animPoints.last().time,
                                  exportData.find("startValuex" + QString::number(i)).value().toString(),
                                  exportData.find("path" + QString::number(i)).value().toString(),
                                  animPoints.last().time,
                                  "x");

        out << exportData.find("part4").value().toString().toUtf8();
        out << parseRawExportData(exportData.find("part5").value().toString(), fileName, animPoints.last().time,
                                  exportData.find("startValuey" + QString::number(i)).value().toString(),
                                  exportData.find("path" + QString::number(i)).value().toString(),
                                  0,
                                  "y");
        for(auto rec : DatabaseManager::getInstance()->getRotationsBySaveId(animationId, i)){
            out << parseRawExportData(exportData.find("part5").value().toString(), fileName, animPoints.last().time,
                                      QString::number(static_cast<double>(rec.y)),
                                      exportData.find("path" + QString::number(i)).value().toString(),
                                      rec.time,
                                      "y");
        }

        out << parseRawExportData(exportData.find("part5").value().toString(), fileName, animPoints.last().time,
                                  exportData.find("startValuey" + QString::number(i)).value().toString(),
                                  exportData.find("path" + QString::number(i)).value().toString(),
                                  animPoints.last().time,
                                  "y");

        out << parseRawExportData(exportData.find("part6").value().toString(), fileName, animPoints.last().time,
                                  exportData.find("startValuey" + QString::number(i)).value().toString(),
                                  exportData.find("path" + QString::number(i)).value().toString(),
                                  animPoints.last().time,
                                  "y");

        out << exportData.find("part4").value().toString().toUtf8();
        out << parseRawExportData(exportData.find("part5").value().toString(), fileName, animPoints.last().time,
                                  exportData.find("startValuez" + QString::number(i)).value().toString(),
                                  exportData.find("path" + QString::number(i)).value().toString(),
                                  0,
                                  "z");
        for(auto rec : DatabaseManager::getInstance()->getRotationsBySaveId(animationId, i)){
            out << parseRawExportData(exportData.find("part5").value().toString(), fileName, animPoints.last().time,
                                      QString::number(static_cast<double>(rec.z)),
                                      exportData.find("path" + QString::number(i)).value().toString(),
                                      rec.time,
                                      "z");
        }

        out << parseRawExportData(exportData.find("part5").value().toString(), fileName, animPoints.last().time,
                                  exportData.find("startValuez" + QString::number(i)).value().toString(),
                                  exportData.find("path" + QString::number(i)).value().toString(),
                                  animPoints.last().time,
                                  "z");

        out << parseRawExportData(exportData.find("part6").value().toString(), fileName, animPoints.last().time,
                                  exportData.find("startValuez" + QString::number(i)).value().toString(),
                                  exportData.find("path" + QString::number(i)).value().toString(),
                                  animPoints.last().time,
                                  "z");
    }
    out << "\nm_EulerEditorCurves:";

    for (int i : bonesNumbers){
        out << parseRawExportData(exportData.find("part7").value().toString(), fileName, animPoints.last().time,
                                  exportData.find("startValuez" + QString::number(i)).value().toString(),
                                  exportData.find("path" + QString::number(i)).value().toString(),
                                  animPoints.last().time,
                                  "x");
        out << parseRawExportData(exportData.find("part7").value().toString(), fileName, animPoints.last().time,
                                  exportData.find("startValuez" + QString::number(i)).value().toString(),
                                  exportData.find("path" + QString::number(i)).value().toString(),
                                  animPoints.last().time,
                                  "y");
        out << parseRawExportData(exportData.find("part7").value().toString(), fileName, animPoints.last().time,
                                  exportData.find("startValuez" + QString::number(i)).value().toString(),
                                  exportData.find("path" + QString::number(i)).value().toString(),
                                  animPoints.last().time,
                                  "z");
    }
    file.close();
}

QString AnimationsManager::parseRawExportData(QString data, QString animationName, int animationLength, QString valuex, QString valuey, QString valuez, QString path, int time){
    data.replace("-animationName-", animationName);
    data.replace("-animationTime-", QString::number(static_cast<double>(animationLength) / 1000));
    data.replace("-valuex-", valuex);
    data.replace("-valuey-", valuey);
    data.replace("-valuez-", valuez);
    data.replace("-path-", path);
    data.replace("-time-", QString::number(static_cast<double>(time) / 1000));
    return data;
}

QString AnimationsManager::parseRawExportData(QString data, QString animationName, int animationLength, QString value, QString path, int time, QString axis){
    data.replace("-animationName-", animationName);
    data.replace("-animationTime-", QString::number(static_cast<double>(animationLength) / 1000));
    data.replace("-value-", value);
    data.replace("-path-", path);
    data.replace("-axis-", axis);
    data.replace("-time-", QString::number(static_cast<double>(time) / 1000));
    return data;
}
