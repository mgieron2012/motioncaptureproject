#ifndef LIGHT_H
#define LIGHT_H

#include<Qt3DCore/QEntity>
#include<Qt3DRender/QDirectionalLight>
#include<Qt3DCore/QTransform>

class Light
{
    Qt3DCore::QEntity *lightEntity;
    Qt3DRender::QDirectionalLight *light;
    Qt3DCore::QTransform *lightTransform;

public:
    Light(Qt3DCore::QEntity*, QVector3D);
};

#endif // LIGHT_H
