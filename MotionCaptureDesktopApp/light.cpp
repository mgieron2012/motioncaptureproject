#include "light.h"

Light::Light(Qt3DCore::QEntity* rootEntity, QVector3D direction)
{
    lightEntity = new Qt3DCore::QEntity(rootEntity);
    light = new Qt3DRender::QDirectionalLight(lightEntity);
    light->setColor("white");
    light->setIntensity(2);
    light->setWorldDirection(direction);
    lightEntity->addComponent(light);
    lightTransform = new Qt3DCore::QTransform(lightEntity);
    lightTransform->setTranslation(QVector3D(0,0,0));
    lightEntity->addComponent(lightTransform);
}
