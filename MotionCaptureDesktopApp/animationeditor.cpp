#include "animationeditor.h"

AnimationEditor::AnimationEditor(NetworkServer* m_server):
    server(m_server)
{
    myName = new QLabel();
    myName->setStyleSheet("font-size: 17px; margin-top: 10px; color: #2d3436;");
    myName->setText("ANIMATION EDITOR");
    addWidget(myName);
    createLayout();
}

void AnimationEditor::createLayout(){
    buttonsLayout = new QHBoxLayout();

    startButton = new QPushButton();
    startButton->setStyleSheet("QPushButton{background-color: #26de81; color: #fff;} QPushButton:hover{background-color: #30d187;}");
    startButton->setText("Start");

    cutButton = new QPushButton();
    cutButton->setStyleSheet("QPushButton{background-color: #f7b731; color: #fff;} QPushButton:hover{background-color: #ffaf38;}");
    cutButton->setText("Cut");

    deleteButton = new QPushButton();
    deleteButton->setStyleSheet("QPushButton{background-color: #eb3b5a; color: #fff;} QPushButton:hover{background-color: #c33b4a;}");
    deleteButton->setText("Delete");

    slider.first = new QSlider();
    slider.second = new QSlider();

    slider.first->setOrientation(Qt::Orientation::Horizontal);
    slider.second->setOrientation(Qt::Orientation::Horizontal);

    slider.first->setVisible(false);
    slider.second->setVisible(false);

    buttonsLayout->addWidget(startButton);
    buttonsLayout->addWidget(cutButton);
    buttonsLayout->addWidget(deleteButton);

    this->addLayout(buttonsLayout);
    this->addWidget(slider.first);
    this->addWidget(slider.second);

    QObject::connect(startButton, &QPushButton::clicked, this, &AnimationEditor::onStartClicked);
    QObject::connect(cutButton, &QPushButton::clicked, this, &AnimationEditor::onCutClicked);
    QObject::connect(deleteButton, &QPushButton::clicked, this, &AnimationEditor::deleteSave);
}


void AnimationEditor::onStartClicked(){
    if (startButton->text() == "Start") startSaving();
        else stopSaving();
}

void AnimationEditor::startSaving(){
    startButton->setText("Stop");
    animationSaver = new AnimationSaver(currentSaveId, server);
}

void AnimationEditor::stopSaving(){
    startButton->setText("Start");
    delete animationSaver;
}

void AnimationEditor::deleteSave(){
    DatabaseManager::getInstance()->deleteSave(currentSaveId);
    emit reloadSaveLabels();
}

void AnimationEditor::onCutClicked(){
    if(slider.first->isVisible()){
        slider.first->setVisible(false);
        slider.second->setVisible(false);
        emit networkSwitch(true);
        cutAnimation(currentSaveId, slider.first->value(), slider.second->value());
    } else {
        slider.first->setVisible(true);
        slider.second->setVisible(true);
        emit networkSwitch(false);
    }
}

void AnimationEditor::cutAnimation(int currentSave, int beggining, int end){
    DatabaseManager::getInstance()->deleteRotation(currentSave, beggining, end);
    emit cutDone();
}

void AnimationEditor::animationChanged(int id, int length){
    currentSaveId = id;
    slider.first->setRange(0, length);
    slider.second->setRange(0, length);
    slider.second->setValue(0);
    slider.first->setValue(0);
}

