#include "rotationdatalabel.h"

RotationDataLabel::RotationDataLabel(QWidget* parentWidget, int m_id)
    :id(m_id),
     isActive(false)
{
    this->setParent(parentWidget);
    setStyleSheet("background-color: #0984e3; color: #fff; border-radius: 5px; padding: 5px; margin: 5px;");
}

void RotationDataLabel::setActive(bool isActive){
    this->isActive = isActive;
}

void RotationDataLabel::setRotation(float x, float y, float z){
    rotation = new QVector3D(x, y, z);
    isActive = true;
    refresh();
}

void RotationDataLabel::refresh(){
    if(isActive)
    setText("Id: " + QString::number(id) +
            "\nStatus: enabled" +
            "\nRotation X:  " + QString::number(static_cast<double>(rotation->x())) +
            "\nRotation Y:  " + QString::number(static_cast<double>(rotation->y())) +
            "\nRotation Z:  " + QString::number(static_cast<double>(rotation->z())));
    else setText("Id: " + QString::number(id) +
                 "\nStatus: disabled" +
                 "\nRotation X: - \nRotation Y: - \nRotation Z: -");
}
