#ifndef BONE_H
#define BONE_H

#include <QObject>
#include <Qt3DCore>
#include <Qt3DExtras>
#include "utils.h"

class Bone : public QObject
{
    Q_OBJECT

public:
    enum ChildType{
        toptop,         //parent top connect to child top
        topbottom,      //parent top connect to child bottom
        bottomtop,      //parent bottom connect to child top
        bottombottom    //parent bottom connect to child bottom
    };
private:
    Qt3DCore::QEntity *entity;
    Qt3DRender::QMesh *mesh;
    Qt3DCore::QTransform *transform;
    Qt3DExtras::QPhongMaterial *material;
    Bone *parent;
    QVector<Bone*> children;
    bool isStatic;      //Enable/Disable rotating
    ChildType childType;

public:
    Bone(Qt3DCore::QEntity *root, Bone* parent);
    void setPosition(float, float, float);
    void setPosition();
    void setScale(float);
    void setRotation(float, float, float);
    void setActive(bool);
    void setStatic(bool);
    void setChildType(ChildType);
    QVector3D getPosition();
    QQuaternion getRotation();
    float getScale(float);
    void addChild(Bone*);

signals:
    void rotationChanged(float, float, float);
};

#endif // BONE_H
